package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals.Option

class OptionalsTest {
  import Option._
  val s1: Option[Int] = Some(1)
  val s2: Option[Int] = Some(2)
  val s3: Option[Int] = None()

  @Test def getOrElseTest(): Unit ={
    assertEquals(1, getOrElse(s1,0))
    assertNotEquals(1, getOrElse(s3,0))
    assertEquals(0, getOrElse(s3,0))
  }

  @Test def filterTest(): Unit ={
    assertEquals(Some(5), filter(Some(5))(_ > 2))
    assertNotEquals(Some(5), filter(Some(5))(_ > 8))
    assertEquals(None(), filter(Some(5))(_ > 8))
  }

  @Test def mapTest(): Unit ={
    assertEquals(Some(true), map(Some(5))(_ > 2))
    assertNotEquals(Some(true), map(Some(5))(_ > 8))
    assertEquals(None(), map(None[Int])(_ > 2))

  }

  @Test def floatMapTest(): Unit ={
    assertEquals(Some(2), flatMap(s1)(i => Some(i+1)))
    assertEquals(Some(3), flatMap(s1)(i => flatMap(s2)(j => Some(i+j))))
    assertNotEquals(Some(3), flatMap(s1)(i => flatMap(s3)(j => Some(i+j))))
    assertEquals(None(), flatMap(s1)(i => flatMap(s3)(j => Some(i+j))))
  }

}
