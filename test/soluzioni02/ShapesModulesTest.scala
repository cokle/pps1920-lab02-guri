package soluzioni02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import soluzioni02.ShapesModules._

class ShapesModulesTest {

  import Shape._
  val square: Shape = Square(5)
  val rectangle: Shape = Rectangle(4, 5)
  val circle: Shape = Circle(6)

  @Test def squareAreaTest(): Unit ={
    assertEquals(25, area(square))
  }

  @Test def rectangleAreaTest(): Unit ={
    assertEquals(20, area(rectangle))
  }

  @Test def circleAreaTest(): Unit ={
    val expected = 113.09
    val DELTA = area(circle) - expected

    assertEquals(expected, area(circle), DELTA)
  }

  @Test def squarePerimeterTest(): Unit ={
    assertEquals(20, perimeter(square))
  }

  @Test def rectanglePerimeterTest(): Unit ={
    assertEquals(18, perimeter(rectangle))
  }

  @Test def circlePerimeterTest(): Unit ={
    val expected = 	37.69
    val DELTA = perimeter(circle) - expected

    assertEquals(expected, perimeter(circle), DELTA)
  }

}
