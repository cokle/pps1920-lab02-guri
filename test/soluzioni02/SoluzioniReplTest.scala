package soluzioni02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class SoluzioniReplTest {

  import SoluzioniREPL.es3._
  import SoluzioniREPL.es4._
  import SoluzioniREPL.es5._
  import SoluzioniREPL.es6._

  // Esercizio 3.a
  @Test def parityValTest(): Unit ={
    assertEquals("even", parity(10))
    assertEquals("odd", parity(11))
    assertNotEquals("odd", parity(10))
  }

  @Test def parityMethodTest(): Unit ={
    assertEquals("even", parity2(10))
    assertEquals("odd", parity2(11))
    assertNotEquals("odd", parity2(10))
  }

  // Esercizio 3.b
  @Test def negValTest(): Unit ={
    val empty: String => Boolean = _==""
    val notEmpty = neg(empty)

    assertTrue(notEmpty("hello"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("hello") && !notEmpty(""))
  }

  @Test def negMethodTest(): Unit ={
    val empty: String => Boolean = _==""
    val notEmpty = neg2(empty)

    assertTrue(notEmpty("hello"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("hello") && !notEmpty(""))
  }

  @Test def negGenericMethodTest(): Unit ={
    val even:  Int=> Boolean = _%2==0
    val notEven = negGeneric(even)

    assertTrue(even(10))
    assertTrue(notEven(11))
    assertTrue(notEven(11) && !notEven(10))
  }

  // Esercizio 4
  @Test def orderedInputValTest(): Unit ={
    assertTrue(orderedInput(1,2,3))
    assertTrue(orderedInput(1,1,3))
    assertFalse(orderedInput(4,1,3))
    assertTrue(orderedInputCurried(1)(2)(3))
    assertTrue(orderedInputCurried(1)(1)(3))
    assertFalse(orderedInputCurried(4)(1)(3))
  }

  @Test def orderedInputMethodTest(): Unit ={
    assertTrue(orderedInput2(1,2,3))
    assertTrue(orderedInput2(1,1,3))
    assertFalse(orderedInput2(4,1,3))
    assertTrue(orderedInputCurried2(1)(2)(3))
    assertTrue(orderedInputCurried2(1)(1)(3))
    assertFalse(orderedInputCurried2(4)(1)(3))
  }

  // Esercizio 5
  @Test def composeValTest(): Unit ={
    assertEquals(9, compose(_-1,_*2)(5))
    assertNotEquals(9, compose(_-1,_*2)(4))
  }

  @Test def composeMethodTest(): Unit ={
    assertEquals(9, compose2(_-1,_*2)(5))
    assertNotEquals(9, compose2(_-1,_*2)(4))
  }

  @Test def composteGenericMethodTest: Unit = {
    assertEquals("Hello Sokol Guri", composeGeneric[String](_ + " Guri", _ + " Sokol")("Hello"))
    assertNotEquals("Hello Sokol Guri", composeGeneric[String](_ + " Sokol", _ + " Guri")("Hello"))
  }
    // Esercizio 6
    @Test def fibMethodTest(): Unit ={
      assertEquals(1, fib(2))
      assertNotEquals(2, fib(2))
    }

}
