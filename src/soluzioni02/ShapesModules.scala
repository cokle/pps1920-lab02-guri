package soluzioni02

object ShapesModules {

  sealed trait Shape
  object Shape {

    case class Square(side: Double) extends Shape
    case class Rectangle(height: Double, weight: Double) extends Shape
    case class Circle(radius: Double) extends Shape

    def area(shape: Shape): Double = shape match {
      case Square(n) => n*n
      case Rectangle(h, w) => h*w
      case Circle(r) =>  r*r * math.Pi
    }

    def perimeter(shape: Shape): Double = shape match {
      case Square(n) => 4*n
      case Rectangle(h, w) => 2*h + 2*w
      case Circle(r) =>  2*r*math.Pi
    }

  }
}
