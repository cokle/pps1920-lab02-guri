
// Esercizio 3.a
    def parity(x: Int): String = (x % 2) match {
        case 0 => "even"
        case _ => "odd"
    }

    val parity2: Int => String =  x => ( x % 2) match {
        case 0 => "even"
        case _ => "odd"
    }


// Esercizio 3.b
    val neg: (String=>Boolean) => (String=>Boolean) = (predicate) => (string => (!predicate(string)))
    def neg2(predicate: String => Boolean) : String => Boolean = (string: String) => !predicate(string)


// Esercizio 3.c + Testing
    def negGeneric[T](predicate: T => Boolean) : T => Boolean = (elem: T) => !predicate(elem)

    val even: (Int => Boolean) = _%2==0 // predicate on even numbers
    val odd = negGeneric(even)          // predicate on odd numbers
    println(even(10))
    println(even(11))
    println(odd(11))


// Esercizio 4
    val orderedInput: (Int, Int, Int) => Boolean = (x, y, z) => (x<=y && y<=z)
    val orderedInputCurried: Int => Int=> Int => Boolean = x => y => z => x<=y && y<=z

    def orderedInput2(x: Int, y: Int, z: Int): Boolean = (x<=y) && (y<=z)
    def orderedInputCurried2(x: Int)(y: Int)( z: Int): Boolean = x<=y && y<=z


// Esercizio 5
    var compose: (Int=>Int, Int=>Int) => Int => Int =  (func1, func2) => value => func1(func2(value))
    def compose2(func1: Int=>Int, func2: Int=>Int): (Int=>Int) = value => func1(func2(value))
    def composeGeneric[T](func1: T=>T, func2: T=>T): (T=>T)  = value => func1(func2(value))

    // When we will call the method, we should specify the type of the parameters that will be used.
    println(composeGeneric[String]( _+ " guri", _+" sokol")("Io sono"))


// Esercizio 6
    // A recursive function is tail recursive if the final result of the recursive call is the final result of the function itself.
    // This isn't a tail recursion because the calling function continues the computation after making the recursive call, the sum.
    // In fact it continues to work on "fib(n-1)  branch" and than continues to the other " branch fib(n-2) ".
    val fib: Int => Int = n => n match  {
        case 0 => 0
        case 1 => 1
        case n => fib(n-1) + fib(n-2)
    }